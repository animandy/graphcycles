#pragma once
#include "StdAfx.h"
#include <vector>
using namespace std;

/*!
*	\file findCyclesFromPoint.h
*	\brief The file includes a function findCyclesFromPoint
*/

/*!
*	\brief The function of searching cycles of a graph, which is held from one point 
*	\param[in] start - start point of the graph traversal
*	\param[in] cur - current point of the graph traversal
*	\param[in] graph - graph
*	\param[in,out] path - the path that you made when you searching cycles from the starting point
*	\param[in,out] result - the list of found cycles
*	\param[in,out] checkedNodes - the list of traversed points of the graph
*/
void findCyclesFromPoint (int start, int cur, vector<vector<int> >graph, vector<int> &path, vector<vector<int> >&result, vector<bool> &checkedNodes);