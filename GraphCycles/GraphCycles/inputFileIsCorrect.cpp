#include "stdafx.h"
#include "inputFileIsCorrect.h"
using namespace std;

/*!
*	\file inputFileIsCorrect.cpp
*	\brief The file includes a function inputFileIsCorrect
*/

bool inputFileIsCorrect(char* filename)
{
	vector<string> str_arr;//������ ����� � ������� �������� �����
	bool isCorrect=true;//���� ������ ������� ������
	int maxPoint=0;//����� ��������� ������� �����
	string tmp="";//������ ��� �������� ��������� ������
	size_t position;//������� ���������� ������� � ������
	char number1[5];//������ ��� �������� ����� �� ������
	char number2[5];//������ ��� �������� ����� �� ������
	ifstream inFile(filename);//����� �������� ������ �������� �����
	
	if(inFile)//���� ����� ������
	{
		//���������� ����� �� �����
		while(getline(inFile, tmp))
		{	//���� �� ����� �� ����� �����
			str_arr.insert(str_arr.end(), tmp);//�������� ������ � ����� ������ ����� �������� �����
		}
		maxPoint=str_arr.size()-1;//����������� ������ ��������� ������� �����
		//�������� �� ������� �����
		if(str_arr.empty())
		{//���� ������ ����� �������� ����� ����
			printf("Error. File is empty\n");//������ ��������� �� ������
			isCorrect=false;//����� ���� ���������� ������� ������
		}

		//�������� ������������ ����� �����
		for(size_t j=0; j<str_arr.size(); j++)
		{//��� ���� ����� �������� �����
			//������ �� ������ ���� ������
			//�������� ������ ����� �������� �����
			if(str_arr[j].size()<1)
			{//���� ������ ������ ����� ������ 1
				printf("Error. String %d. Empty string\n", j);//������ ��������� �� ������
				isCorrect=false;//����� ���� ���������� ������� ������
			}
			//���� ������ ������ - �� ���� "�����" ��� �� �����, �� ������ ��������
			if(!isdigit(str_arr[j][0]))
				if(str_arr[j][0]!='-')
				{
					printf("Error. String �%d. Wrong symbols. Correct symbols are numbers and space symbols\n", j);//������ ��������� �� ������
					isCorrect=false;//����� ���� ���������� ������� ������
				}
			//���� ������ ������ - ���� "�����", �� ����� ���� ������ ������ ������ "1"
			if(str_arr[j][0]=='-')//���� ������ ������ ������ - ���� "�����"
				if(!(str_arr[j].size()==2 && str_arr[j][1]!='1'))
				{//���� ����� ������� ������� �� ����� ������ "1"
					printf("error. String �%d. String has got negative number. You can input '-1' if current node has no outgoing edges \n", j);//������ ��������� �� ������
					isCorrect=false;//����� ���� ���������� ������� ������
				}
			/*���� ������ ������ - �����, �� � ������ �� ������ ���� �����,�������� ������� ������, ��� ����������
			������ �����, � ������ �� ������ ���� ������������� �����, ������ ������ �������� ������ �� ����� � ��������� ����������� 
			������ ' '(��������) ����� �������. 	
			*/
			if(isdigit(str_arr[j][0]))	
			{//���� ������ ������ - �����
				//�������� ����, ��� ������ ������� ������ �� ���� � ��������
				for(size_t i=0; i<str_arr[j].size(); i++)
				{//��� ���� �������� ������
					if(!(isdigit(str_arr[j][i]) || str_arr[j][i]==' '))
					{//���� ������� ������ �� ����� ��� �� ������
						printf("Error. String %d. String has got wrong symbols. Correct symbols are numbers and space symbols\n", j);//������ ��������� �� ������
						isCorrect=false;//����� ���� ���������� ������� ������
					}
				}
				//�������� ������������� �����.
				for(int i=0; i<maxPoint+1; i++)//��� ���� �����, ������� ��� ����� ��������� ������� �����
					if(str_arr[j].find_first_of(_itoa_s(i, number1,5, 10))!=str_arr[j].find_last_of(_itoa_s(i, number2,5, 10)))
					{//���� ������� ����������� � ������ ������ ������ ����
						printf("error. String %d. String has got similar number\n", j);//������ ��������� �� ������
						isCorrect=false;//����� ���� ���������� ������� ������
					}
				//�������� ����� �������� ����� �� ������� ���������� ������
				for(size_t i=0; i<str_arr[j].size(); i++)
				{//��� ���� �������� ������
					tmp="";//�������� ��������� ������
					//����������� ����� � ������
					while(isdigit(str_arr[j][i]) && i<str_arr[j].size())
					{//���� ������� ������ - ����� � �� ����� �� ����� ������
						tmp+=str_arr[j][i];//�������� ����� �� ��������� ������
						i++;//������� � ���������� �������
					}
					if(atoi(tmp.c_str())>maxPoint)
					{//���� ��������� ����� ������ ������ ��������� ������� ����� 
						printf("error. String %d. Input points don't exist. You entered a number whose value is greater than the number of vertices of the graph\n", j);//������ ��������� �� ������
						isCorrect=false;//����� ���� ���������� ������� ������
					}		
				}
				//�������� ��������� ��������
				position=str_arr[j].find(" ");//����� ������� ������� ������� � ������
				while(position!=string::npos)//���� �� ����� �� ����� �����
				{
					if(position==str_arr[j].size()-1)//���� ������ �������� ��������� �������� ������
					{
						printf("error. String %d. Last symbol must be number\n", j);//������ ��������� �� ������
						isCorrect=false;//����� ���� ���������� ������� ������
					}
					else	
					{
						if(!isdigit(str_arr[j][position+1]))//���� ����� ������� �� ����� �����
						{
							printf("Error. String %d. Too much alpha symbols are in row. You can input only single space symbols between numbers\n", j);//������ ��������� �� ������
							isCorrect=false;//����� ���� ���������� ������� ������
						}
						position=str_arr[j].find(" ", position+1);//������� � ���������� ������� ������
					}
				}
			}
				
		}
	}
	else
		printf("Error. Input file don't exist\n");//������ ��������� � ���, ��� ������� ���� �� ������	
		
	inFile.close();	//������� ����� ������
	return isCorrect;
}
