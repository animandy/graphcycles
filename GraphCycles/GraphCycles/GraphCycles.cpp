// GraphCycles.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "findAllCycles.h"
#include "findCyclesFromPoint.h"
#include "getInputData.h"
#include "inputFileIsCorrect.h"
#include <vector>
#include <fstream>
#include <iostream>
#include <string>
#include <cstring>
#include <cctype>
#include <cstddef> 
using namespace std;

/*!\mainpage graphCycles
*
*This program allows to determine
*whether there are cycles in this directed graph
*and find them
*
*creator: Emelyanov Andrey
*/

/*!
*\file GraphCycles.cpp
*\brief The file includes a function _tmain
*/

/*!
*	\brief Function of running the program
*	\param[in] argc - the number of arguments
*	\param[in] argv - the list of arguments
*	\return 0
*/

int main(int argc, char* argv[])
{
	vector<vector<int> >graph;//����
	vector<vector<int> >result;//������ ��������� ������
	bool isCorrect=true;//���� ������ ������� ������
	vector<int>::iterator iterResult;//�������� ������� ��������� ������

	
	if (argc!=3) //���� ���������� ���������� �� ����� ���
	{
		printf("Error. Wrong number of arguments. You must input name of executable file, name of input file, name of output file\n");//������ ��������� �� ������
		isCorrect=false;//����� ���� ���������� ������� ������
	}
	else
	{	
		isCorrect=inputFileIsCorrect(argv[1]);//������� ���� ���������� ������� ������
		ofstream outFile(argv[2]);//����� �������� ������ ��������� �����
		if(isCorrect && outFile)//���� ������� ������ ����� � ����� ������ 
 		{
			getInputData(argv[1], graph);//�������� ������� ������ �� �����
			findAllCycles(graph, result);//����� ����� �����
			//������� ���������� ����������
			if(result.empty())//���� ����� �� �������
				outFile<<"There is no cycles in graph\n";//������� ��������� � ���, ��� ����� �� �������
			else
			{
				outFile<<"There are cycles in graph\n"<<"number of cicles: "<<result.size()<<"\n";//������� ��������� � ���, ��� ����� ������� � ������� ������ �������
				//����� ��������� ������
				for(size_t i=0; i<result.size(); i++)
				{//��� ���� ��������� ������
					iterResult=result[i].begin();//��������� �������� �� ������ ������ �����������
					while(iterResult!=result[i].end())//�� ����� ������
					{
						outFile<<*iterResult<<" ";//������� ������� �����
						iterResult++;//������� � ��������� �������
					}
					outFile<<"\n";//������� � ������ � ��������� ������ �����
				}
			}
		}
		outFile.close();	//������� ����� ������
	}
	
	return 0;
}

