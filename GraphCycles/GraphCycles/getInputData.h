#pragma once
#include "StdAfx.h"
#include <vector>
#include <fstream>
#include <string>
#include <cstring>
#include <cctype>
#include <cstddef> 
using namespace std;

/*!
*	\file getInputData.h
*	\brief The file includes a function getInputData
*/

/*!
*	\brief Function of receiving input data from the input file
*	\param[in] filename - the name of the input file
*	\param[in,out] graph - graph
*/
void getInputData(char* filename, vector<vector<int> >&graph);