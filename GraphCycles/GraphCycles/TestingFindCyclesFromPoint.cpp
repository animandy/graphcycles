#include "StdAfx.h"
#include <cfixcc.h>
#include <vector>
#include "findCyclesFromPoint.h"
using namespace std;

class TestingFindCyclesFromPoint : public cfixcc::TestFixture
{
private:

public:
	void Test1OnePointNoCycles()
	{
		vector< vector<int> > graph;
		vector<int> path;
		vector< vector<int> > result;
		vector<bool> CheckedNodes;

		CheckedNodes.push_back(false);

		findCyclesFromPoint(0, 0, graph, path, result, CheckedNodes);
		CFIX_ASSERT_EQUIALS(0, result[0].size());
		CFIX_ASSERT_EQUIALS(true, CheckedNodes[0]);
	}

	void Test2OnePointOneCycle()
	{
		vector< vector<int> > graph;
		vector<int> path;
		vector< vector<int> > result;
		vector<bool> CheckedNodes;
		vector<int> graph1(1,0);
		graph.push_back(graph1);
		CheckedNodes.push_back(false);

		findCyclesFromPoint(0, 0, graph, path, result, CheckedNodes);
		CFIX_ASSERT_EQUIALS(1, result[0].size());
		CFIX_ASSERT_EQUIALS(0, result[0][0]);
		CFIX_ASSERT_EQUIALS(true, CheckedNodes[0]);
	}

	void Test3ThreePointsNoCycles()
	{
		vector< vector<int> > graph;
		vector<int> path;
		vector< vector<int> > result;
		vector<bool> CheckedNodes(3, false);
		vector<int> graph1(1,1);
		vector<int> graph2(1,2);
		vector<int> graph3;
		graph.push_back(graph1);
		graph.push_back(graph2);
		graph.push_back(graph3);
		

		findCyclesFromPoint(0, 0, graph, path, result, CheckedNodes);
		CFIX_ASSERT_EQUIALS(0, result[0].size());
		CFIX_ASSERT_EQUIALS(true, CheckedNodes[0]);
		CFIX_ASSERT_EQUIALS(true, CheckedNodes[1]);
		CFIX_ASSERT_EQUIALS(true, CheckedNodes[2]);

	}

	void Test4ThreePointsOneCycle()
	{
		vector< vector<int> > graph;
		vector<int> path;
		vector< vector<int> > result;
		vector<bool> CheckedNodes(3, false);
		vector<int> graph1(1,1);
		vector<int> graph2(1,2);
		vector<int> graph3(1,0);
		graph.push_back(graph1);
		graph.push_back(graph2);
		graph.push_back(graph3);
		

		findCyclesFromPoint(0, 0, graph, path, result, CheckedNodes);
		CFIX_ASSERT_EQUIALS(3, result[0].size());
		CFIX_ASSERT_EQUIALS(0, result[0][0]);
		CFIX_ASSERT_EQUIALS(1, result[0][1]);
		CFIX_ASSERT_EQUIALS(2, result[0][2]);
		CFIX_ASSERT_EQUIALS(true, CheckedNodes[0]);
		CFIX_ASSERT_EQUIALS(true, CheckedNodes[1]);
		CFIX_ASSERT_EQUIALS(true, CheckedNodes[2]);
	}

	void Test5FivePointsThreeCycles()
	{
		vector< vector<int> > graph;
		vector<int> path;
		vector< vector<int> > result;
		vector<bool> CheckedNodes(5, false);
		vector<int> graph1;
		graph1.push_back(1);
		graph1.push_back(3);
		vector<int> graph2(1,2);
		vector<int> graph3(1,0);
		vector<int> graph4(1,4);
		vector<int> graph5(1,0);

		graph.push_back(graph1);
		graph.push_back(graph2);
		graph.push_back(graph3);
		graph.push_back(graph4);
		graph.push_back(graph5);
		

		findCyclesFromPoint(0, 0, graph, path, result, CheckedNodes);
		CFIX_ASSERT_EQUIALS(3, result[0].size());
		CFIX_ASSERT_EQUIALS(3, result[1].size());
		CFIX_ASSERT_EQUIALS(5, result[2].size());
		CFIX_ASSERT_EQUIALS(0, result[0][0]);
		CFIX_ASSERT_EQUIALS(1, result[0][1]);
		CFIX_ASSERT_EQUIALS(2, result[0][2]);
		CFIX_ASSERT_EQUIALS(0, result[1][0]);
		CFIX_ASSERT_EQUIALS(3, result[1][1]);
		CFIX_ASSERT_EQUIALS(4, result[1][2]);
		CFIX_ASSERT_EQUIALS(0, result[2][0]);
		CFIX_ASSERT_EQUIALS(1, result[2][1]);
		CFIX_ASSERT_EQUIALS(2, result[2][2]);
		CFIX_ASSERT_EQUIALS(0, result[2][3]);
		CFIX_ASSERT_EQUIALS(3, result[2][4]);
		CFIX_ASSERT_EQUIALS(4, result[2][5]);
		CFIX_ASSERT_EQUIALS(true, CheckedNodes[0]);
		CFIX_ASSERT_EQUIALS(true, CheckedNodes[1]);
		CFIX_ASSERT_EQUIALS(true, CheckedNodes[2]);
		CFIX_ASSERT_EQUIALS(true, CheckedNodes[3]);
		CFIX_ASSERT_EQUIALS(true, CheckedNodes[4]);
	}

	void Test6FivePointsOneFoundedCycle()
	{
		vector< vector<int> > graph;
		vector<int> path;
		vector< vector<int> > result;
		vector<bool> CheckedNodes(5, false);
		vector<int> graph1(1,1);
		vector<int> graph2(1,2);
		vector<int> graph3(1,0);
		vector<int> graph4(1,4);
		vector<int> graph5(1,3);

		graph.push_back(graph1);
		graph.push_back(graph2);
		graph.push_back(graph3);
		graph.push_back(graph4);
		graph.push_back(graph5);
		

		findCyclesFromPoint(0, 0, graph, path, result, CheckedNodes);
		CFIX_ASSERT_EQUIALS(3, result[0].size());
		CFIX_ASSERT_EQUIALS(0, result[0][0]);
		CFIX_ASSERT_EQUIALS(1, result[0][1]);
		CFIX_ASSERT_EQUIALS(2, result[0][2]);
		CFIX_ASSERT_EQUIALS(true, CheckedNodes[0]);
		CFIX_ASSERT_EQUIALS(true, CheckedNodes[1]);
		CFIX_ASSERT_EQUIALS(true, CheckedNodes[2]);
		CFIX_ASSERT_EQUIALS(true, CheckedNodes[3]);
		CFIX_ASSERT_EQUIALS(true, CheckedNodes[4]);

	}


};

CFIXCC_BEGIN_CLASS(TestingFindCyclesFromPoint)
	CFIXCC_METHOD(Test1OnePointNoCycles)
	CFIXCC_METHOD(Test2OnePointOneCycle)
	CFIXCC_METHOD(Test3ThreePointsNoCycles)
	CFIXCC_METHOD(Test4ThreePointsOneCycle)
	CFIXCC_METHOD(Test5FivePointsThreeCycles)
	CFIXCC_METHOD(Test6FivePointsOneFoundedCycle)
CFIXCC_END_CLASS()

