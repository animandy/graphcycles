#include "stdafx.h"
#include "findAllCycles.h"
#include "findCyclesFromPoint.h"
using namespace std;

/*!
*	\file findAllCycles.cpp
*	\brief The file includes a function findAllCycles
*/

void findAllCycles(vector<vector<int> >graph, vector<vector<int> >&result)
{
	int count=0;//������� ������������ �������
	vector<bool> checkedNodes(graph.size(), false);//c����� ���������� ������ �����
	vector<bool>::iterator iter;//�������� ������ ���������� ������ �����
	vector<int> path;//����, ����������� ��� ������ �����
	iter=checkedNodes.begin();//��������� �������� �� ������ ������
	//����� ������
	while(iter!=checkedNodes.end())
	{//���� �� ����� �� ����� ������ ���������� ������
		if(*iter==false)//���� ������� ����������, ��� ������������
			findCyclesFromPoint(count, count, graph, path, result, checkedNodes);//�������� ����� ������ �� ������� ������������ �������
		//������� � ��������� �������
		count++;
		iter++;
		path.clear();//�������� ������ ���������� ������
	}
}