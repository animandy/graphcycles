#include "StdAfx.h"
#include <cfixcc.h>
#include <vector>
#include "findAllCycles.h"
using namespace std;
class TestingFindAllCycles : public cfixcc::TestFixture
{
private:

public:
	void Test1FivePointsTwoCycles()
	{
		vector<vector<int> >graph;
		vector<vector<int> >result;
		vector<int>graph1(1,1);
		vector<int>graph2(1,2);
		vector<int>graph3(1,0);
		vector<int>graph4(1,4);
		vector<int>graph5(1,3);
		graph.push_back(graph1);
		graph.push_back(graph2);
		graph.push_back(graph3);
		graph.push_back(graph4);
		graph.push_back(graph5);
		
		findAllCycles(graph, result);

		CFIX_ASSERT_EQUIALS(3, result[0].size());
		CFIX_ASSERT_EQUIALS(2, result[1].size());
		CFIX_ASSERT_EQUIALS(0, result[0][0]);
		CFIX_ASSERT_EQUIALS(1, result[0][1]);
		CFIX_ASSERT_EQUIALS(2, result[0][2]);
		CFIX_ASSERT_EQUIALS(3, result[1][0]);
		CFIX_ASSERT_EQUIALS(4, result[1][1]);
	
	}

	void Test2FivePointsThreeCycles()
	{
		vector<vector<int> >graph;
		vector<vector<int> >result;
		vector<int>graph1;
		graph1.push_back(1);
		graph1.push_back(3);
		vector<int>graph2(1,2);
		vector<int>graph3(1,0);
		vector<int>graph4(1,4);
		vector<int>graph5(1,0);
		graph.push_back(graph1);
		graph.push_back(graph2);
		graph.push_back(graph3);
		graph.push_back(graph4);
		graph.push_back(graph5);
		
		findAllCycles(graph, result);

		CFIX_ASSERT_EQUIALS(3, result[0].size());
		CFIX_ASSERT_EQUIALS(3, result[1].size());
		CFIX_ASSERT_EQUIALS(5, result[2].size());
		CFIX_ASSERT_EQUIALS(0, result[0][0]);
		CFIX_ASSERT_EQUIALS(1, result[0][1]);
		CFIX_ASSERT_EQUIALS(2, result[0][2]);
		CFIX_ASSERT_EQUIALS(0, result[1][0]);
		CFIX_ASSERT_EQUIALS(3, result[1][1]);
		CFIX_ASSERT_EQUIALS(4, result[1][2]);
		CFIX_ASSERT_EQUIALS(0, result[2][0]);
		CFIX_ASSERT_EQUIALS(1, result[2][1]);
		CFIX_ASSERT_EQUIALS(2, result[2][2]);
		CFIX_ASSERT_EQUIALS(0, result[2][3]);
		CFIX_ASSERT_EQUIALS(3, result[2][4]);
		CFIX_ASSERT_EQUIALS(4, result[2][5]);
	}
	
	void Test3TwoPointsNoCycles()
	{
		vector<vector<int> >graph;
		vector<vector<int> >result;
		vector<int>graph1;
		vector<int>graph2;
		graph.push_back(graph1);
		graph.push_back(graph2);
		CFIX_ASSERT_EQUIALS(0, result[0].size());
	}
	
	void Test4OnePointNoCycles()
	{
		vector<vector<int> >graph;
		vector<vector<int> >result;
		vector<int>graph1;
		graph.push_back(graph1);
		CFIX_ASSERT_EQUIALS(0, result[0].size());
	}
	
	void Test5OnePointoneCycle()
	{
		vector<vector<int> >graph;
		vector<vector<int> >result;
		vector<int>graph1(1, 0);
		graph.push_back(graph1);
		CFIX_ASSERT_EQUIALS(1, result[0].size());
		CFIX_ASSERT_EQUIALS(0, result[0][0]);
	}
};

CFIXCC_BEGIN_CLASS(TestingFindAllCycles)
	CFIXCC_METHOD(Test1FivePointsTwoCycles)
	CFIXCC_METHOD(Test2FivePointsThreeCycles)
	CFIXCC_METHOD(Test3TwoPointsNoCycles)
	CFIXCC_METHOD(Test4OnePointNoCycles)
	CFIXCC_METHOD(Test5OnePointoneCycle)
CFIXCC_END_CLASS()

