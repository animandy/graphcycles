#pragma once
#include "stdafx.h"
#include <vector>
#include <fstream>
#include <string>
#include <cstring>
#include <cctype>
#include <cstddef> 
#include <stdlib.h>

using namespace std;

/*!
*	\file inputFileIsCorrect.h
*	\brief The file includes a function inputFileIsCorrect
*/

/*!
*	\brief Function checks correctness of input data
*	\param[in] filename - the name of the input file
*	\return - mark of correctness of input data. True - input data is correct, false - the input data is incorrect
*/
bool inputFileIsCorrect(char* filename);
