#include "stdafx.h"
#include "getInputData.h"

using namespace std;

/*!
*	\file getInputData.cpp
*	\brief The file includes a function getInputData
*/

void getInputData(char* filename, vector<vector<int> >&graph)
{
	ifstream inFile(filename);//����� ������ �������� �����
	
	string tmp;//��������� ������
	string number;//������ ��� �������� ����� �� �����
	int currentRow=0;//����� ������� ������
	while(getline(inFile, tmp))
	{//��� ���� ����� �����
		graph.push_back(vector<int>());//�������� ������ ������ � ������ �����
		for(int i=0; i<tmp.size(); i++)
		{//��� ���� �������� ���������� ������
			number="";//�������� ������ ��� �����
			while(isdigit(tmp[i]) && i<tmp.size())
			{//���� ������� ������ �������� ������ � �� ����� �� ����� ������			
				number+=tmp[i];//�������� ����� � ����� ������ ��� �����
				i++;//������� � ���������� �������
			}
			if(atoi(number.c_str())!=-1)//���� ��������� ����� �� �������������
				graph[currentRow].push_back(atoi(number.c_str()));//�������� ����� � ������ ������� ������ ��� ������� ������� ����� 
		}
		currentRow++;//������� � ���������� ����
	}

	inFile.close();//������� ����� ������
}