#pragma once
#include <vector>
#include "StdAfx.h"
using namespace std;

/*!
*	\file findAllCycles.h
*	\brief The file includes a function findAllCycles
*/

/*!
*	\brief The function of searching all cycles of the graph
*	\param[in] graph - graph
*	\param[in,out] result - the list of found cycles
*/
void findAllCycles(vector<vector<int> >graph, vector<vector<int> >&result);