#include "stdafx.h"
#include "findCyclesFromPoint.h"

using namespace std;

/*!
*	\file findCyclesFromPoint.cpp
*	\brief The file includes a function findCyclesFromPoint
*/

void findCyclesFromPoint (int start, int cur, vector<vector<int> >graph, vector<int> &path, vector<vector<int> >&result, vector<bool> &checkedNodes)
{
	vector<int>::iterator it;//�������� ������ ������� ������ ��� ������� �������
	vector<int>::iterator it2;//�������� ������ ����������� ����
	bool doRecurs=true;//������� ����, ���� �� ��������� ����� �� ��������� �������
	path.push_back(cur);//�������� ������� ������� � ������ ����������� ����
	checkedNodes[cur]=true;//���������� ������� ������� ��� ���������� � ������ ���������� ������
	it=graph[cur].begin();//��������� �������� �� ������ ������
	//����� ����� �� ������� �������
	while(it!=graph[cur].end())
	{//���� �� ����� �� ����� ������
		if(*it==start)//���� ������� ������� ��� ������� ������� ��������� �� ��������� ��������
 			result.push_back(path);//�������� ���������� ���� � ������ ��������� ������
		it2=path.begin();//��������� �������� �� ������ ������
		//����� ��������� ������� ������
		while(it2!=path.end())
		{//���� �� ����� �� ����� ������
			if(*it==*it2)//���� ������� ������� ������� ��� ������� ������� ������������ � ������ ������
				doRecurs=false;//������� ���� ������ ��� ������� ������� �������
			it2++;//��������� � ��������� ������� ����������� ����
		}
		if (doRecurs )//���� ��������� ���� ����������� ������ �����
			findCyclesFromPoint(start,*it,graph,path,result,checkedNodes);//���������� ����� �� ������� ������� ������� ��� ������� ������� 
		it++;//��������� � ��������� ������� ������� ������� ������� 
	}
}